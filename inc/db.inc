<?php
require_once 'config.inc';

$db = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

mysqli_set_charset($db, 'utf8');

if ($db->connect_errno) {
	echo "Не удалось подключиться к MySQL: " . $db->connect_error;
	die();
}