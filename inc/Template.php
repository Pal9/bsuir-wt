<?php
/**
 * Created by PhpStorm.
 * User: a.kolcov
 * Date: 21.09.2018
 * Time: 15:14
 */

class Template
{

	const CACHE_DIR = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR; // Путь к директории с кэшем
	const CACHE_EXT = 'cch'; // Расширение файлов кэша
	const TPL_DIR =  __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'tpl' . DIRECTORY_SEPARATOR; // Папка, в которой хранятся шаблоны

    protected $template = ''; // Название шаблона
    protected $vars = []; // Переменные для подстановки
    protected $cacheKey = ''; // Расширение для кэша
    protected $cachePath = ''; // Папка, в которой хранится кэш

    function __construct($template, $vars = [])
    {
        $this->template = $template;
        $this->vars = $vars;
        $this->cacheKey = md5(serialize($vars));
        $this->cachePath = self::CACHE_DIR . $this->template . $this->cacheKey . '.' . self::CACHE_EXT;

        if (!file_exists(self::CACHE_DIR)) { // если папка для кэша ещё не создана - создаём её
        	mkdir(self::CACHE_DIR);
        }
    }

    /**
     * Получение данных о шаблоне
     *
     * @return string
     */
    public function load() : string {
        return (string) (file_exists($this->cachePath) ? file_get_contents($this->cachePath) : $this->compile());
    }

    /**
     * "Компиляция" данных по запросу
     *
     * @return string
     */
    protected function compile() : string {
        $source = (string) file_get_contents(self::TPL_DIR . $this->template . '.tpl');
        $result = $this->resolveLoops($source);
        $result = $this->resolveBranches($result);
	    $result = $this->resolveIncludes($result);
	    $result = $this->replaceVars($result);
        file_put_contents($this->cachePath, $result);
        return $result;
    }

	/**
	 * Разворачиваем итерации по массивам
	 *
	 * @param string $source
	 *
	 * @return string
	 */
    protected function resolveLoops(string $source) : string {
	    return (string) preg_replace_callback(
		    "/{{foreach\((?<varName>.*?)\)}}(?<foreachBody>.*?){{endforeach}}/s",
		    function ($matches) {
			    if (isset($this->vars[$matches['varName']]) && is_array($this->vars[$matches['varName']])) {
			    	$result = '';

			    	foreach ($this->vars[$matches['varName']] as $key => $value) {
			    		$tmpVarName = 'loop_' . $key . '_' . $matches['varName'];
			    		$result .= str_replace(
			    			['{{=key}}', '{{=value}}'],
						    [$key, '{{=' . $tmpVarName . '}}'],
						    $matches['foreachBody']
					    );

			    		$this->vars[$tmpVarName] = $value;
				    }

				    return $result;
			    } else {
				    return '';
			    }
		    },
		    $source
	    );
    }

    /**
     * Решаем развилки if-else
     *
     * @param string $source
     * @return string
     */
    protected function resolveBranches(string $source) : string {
	    return (string) preg_replace_callback(
	    	"/{{if\((?<varName>.*?)\)}}(?<ifBody>.*?)({{else}}(?<elseBody>.*?))?{{endif}}/s",
		    function ($matches) {
                if (isset($this->vars[$matches['varName']])) {
                	return $matches['ifBody'];
			    } else {
                	return $matches['elseBody'];
			    }
		    },
		    $source
	    );
    }

    /**
     * Включаем шаблоны, подключенные в шаблон
     *
     * @param string $source
     * @return string
     */
    protected function resolveIncludes(string $source) : string {
	    return (string) preg_replace_callback(
		    "/{{include\((?<template>\S*?)\)({{=(?<varName>.*)}})?}}/",
		    function ($matches) {
		    	$template = new self($matches['template'], isset($matches['varName']) ? $this->vars[$matches['varName']] : $this->vars);
		    	return $template->load();
		    },
		    $source
	    );
    }

	/**
	 * Подстановка переменных $vars в шаблон $data
	 *
	 * @param string $source
	 * @return string
	 */
	protected function replaceVars(string $source) : string {

		// находим все плейсхолдеры, ожидающие подстановки
		preg_match_all('/{{=(?<varName>.*?)}}/', $source, $matches);

		// устанавливаем соответствие плейсхолдеров переданным переменным
		$vars = [];
		if (is_array($matches['varName'])) {
			foreach ($matches['varName'] as $varName) {
				preg_match_all('/(?<arrayName>.*?)\[(?<field>.*?)\]/', $varName, $matches);
				if (count($matches['field'])) { // если в хуке есть что-то в квадратных скобках, ожидается подстановка из массива
					$arrayName = $matches['arrayName'][0];
					$arrayField = $matches['field'][0];

					if (is_array($this->vars[$arrayName])) {
						$vars['{{=' . $varName . '}}'] = $this->vars[$arrayName][$arrayField];
					} else {
						$vars['{{=' . $varName . '}}'] = '';
					}
				} else {
					$vars['{{=' . $varName . '}}'] = $this->vars[$varName];
				}
			}
		}

		return str_replace(array_keys($vars), array_values($vars), $source);
	}

    /**
     * Очищаем файлы кэша. Если хоть один удалён с ошибкой, сообщаем об этом в возвращаемом значении
     *
     * @return bool
     */
    public static function flush() {

        $result = [];
	    $handle = opendir(self::CACHE_DIR);

        while (false !== ($file = readdir($handle))) {
            if (pathinfo($file, PATHINFO_EXTENSION) === self::CACHE_EXT) {
                $result[] = unlink(self::CACHE_DIR . $file);
            }
        }

        closedir($handle);

        return !in_array(false, $result);
    }
}