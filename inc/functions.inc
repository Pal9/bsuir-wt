<?php
/**
 * Рассчитываем размер директории с поддиректориями
 * @param string $dir
 *
 * @return int
 */
function folderSize(string $dir) : int {
	$size = 0;
	foreach (glob(rtrim($dir, '/').'/*', GLOB_NOSORT) as $each) {
		$size += is_file($each) ? filesize($each) : folderSize($each);
	}
	return $size;
}

/**
 * Возвращаем размер объекта, расположенного по пути, в КБ, абстрагируясь от типа объекта
 * @param string $path
 *
 * @return float
 */
function targetSize(string $path) : float {
	return round((is_dir($path) ? folderSize($path) : filesize($path)) / 1024, 2);
}

/**
 * Рассчитываем размер изображений в заданной директории и её поддиректориях
 * @param string $dir
 *
 * @return float
 */
function imagesSize(string $dir) : float {
	$size = 0;
	foreach (glob(rtrim($dir, '/').'/*', GLOB_NOSORT) as $each) {
		if (is_dir($each)) {
			$size += imagesSize($each);
		} elseif (is_file($each) && strpos(mime_content_type($each), 'image/') !== false) {
			$size += filesize($each);
		}
	}
	return round($size / 1024, 2);
}

/**
 * Функция возвращает массив названий дисциплин, по которым балл совпал с заданным
 * @param array $student
 * @param int $targetPoint
 *
 * @return array
 */
function matchDisciplines(array $student, int $targetPoint) : array {

	$list = [];

	if ($targetPoint == $student['math']) {
		$list[] = 'Математика';
	}
	if ($targetPoint == $student['web']) {
		$list[] = 'Веб-технологии';
	}
	if ($targetPoint == $student['history']) {
		$list[] = 'История';
	}
	if ($targetPoint == $student['philosophy']) {
		$list[] = 'Философия';
	}
	if ($targetPoint == $student['english']) {
		$list[] = 'Английский';
	}

	return $list;

}

/**
 * Определяем браузер по useragent
 * @param $agent
 *
 * @return string
 */
function getBrowser($agent) {
	preg_match(
		"/(MSIE|Opera|Firefox|Edge|Chrome|Version|Opera Mini|Netscape|Konqueror|SeaMonkey|Camino|Minefield|Iceweasel|K-Meleon|Maxthon)(?:\/| )([0-9.]+)/",
		$agent,
		$browserInfo
	); // регулярное выражение, которое позволяет отпределить 90% браузеров

	// получаем данные из массива в переменные
	$browser = $browserInfo[1];
	$version = $browserInfo[2];

	if (preg_match("/Opera ([0-9.]+)/i", $agent, $opera)) {
		return 'Opera ' . $opera[1]; // определение _очень_старых_ версий Оперы (до 8.50)
	}

	if ($browser === 'MSIE') { // если браузер определён как IE
		preg_match("/(Maxthon|Avant Browser|MyIE2)/i", $agent, $ie);
		// проверяем, не разработка ли это на основе IE
		if ($ie) {
			return $ie[1].' based on IE ' . $version; // если да, то возвращаем сообщение об этом
		}
		return 'IE ' . $version; // иначе просто возвращаем IE и номер версии
	}

	if ($browser === 'Firefox') {
		// если браузер определён как Firefox // проверяем, не разработка ли это на основе Firefox
		preg_match("/(Flock|Navigator|Epiphany)\/([0-9.]+)/", $agent, $ff);
		if ($ff) {
			return $ff[1].' '.$ff[2]; // если да, то выводим номер и версию
		}
	}

	if ($browser === 'Opera' && $version === '9.80') {
		// если браузер определён как Opera 9.80, берём версию Оперы из конца строки
		return 'Opera ' . substr($agent, -5);
	}

	if ($browser === 'Version') {
		return 'Safari ' . $version; // определяем Сафари
	}

	if ($browser === 'Chrome') {
		// Edge прикидывается Хромом, проверяем, не тот ли это случай
		if (strpos($agent,'Edge')) {
			return 'Edge';
		}
	}

	if (!$browser && strpos($agent, 'Trident')) {
		// для неопознанных браузеров проверяем, если они на движке Trident, вероятно это один из последних IE
		return 'Internet Explorer';
	}

	if (!$browser && strpos($agent, 'Gecko')) {
		// для неопознанных браузеров проверяем, если они на движке Gecko, и возращаем сообщение об этом
		return 'Browser based on Gecko';
	}

	return $browser . ' ' . $version; // для всех остальных возвращаем браузер и версию
}