<?php $title = 'Открытие 4 корпуса'; ?>
<!doctype html>
<html lang="ru">
<head>
	<?php include 'inc/head.inc'; ?>
	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<?php include 'inc/header.inc'; ?>
		<div class="main-row">
			<?php include 'inc/nav.inc'; ?>
			<main>
				<h1><?= $title ?></h1>
                <p>Сегодня состоялось торженственное открытие 4 корпус БГУИР. Виновные наказаны. Уже завтра они приступят к выполнеию своих трудовых обязанностей.</p>
                <p>Напоминаем вам, что открытия, равно как и закрытия - штука цикличная и ничто не вечно, а жизнь безумно скоротечна.</p>
                <img src="/img/bsuir-4.jpg" alt="4 корпус БГУИР" title="4 корпус БГУИР, цифровая фотография на фотоаппарат, сделанная фотографом" class="adaptive-img">
			</main>
		</div>
		<?php include 'inc/footer.inc'; ?>
	</div>
</body>
</html>