<?php $title = 'Лабораторная работа №7, вариант 4'; ?>
<!doctype html>
<html lang="ru">
<head>
	<?php include 'inc/head.inc'; ?>
	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<?php include 'inc/header.inc'; ?>
		<div class="main-row">
			<?php include 'inc/nav.inc'; ?>
			<main>
				<h1><?= $title ?></h1>
                <h2>Задание:</h2>
                <p>Написать скрипт, позволяющий просматривать и редактировать список установленных у пользователя куки.</p>
                <h2>Решение:</h2>
                <p>В задании не сказано в явном виде о том, что речь о куках конкретного учебного сайта, но такое ограничение накладывает сама технология. Поэтому фактически реализован классический CRUD по отношению к Cookies данного учебного сайта. Интерфейс для взаимодействия расположен ниже на этой странице.</p>
                <h3>Список cookies текущего сайта</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Ключ</th>
                        <th>Значение</th>
                        <th>Управление данными</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php foreach ($_COOKIE as $key => $value) { ?>
                        <tr>
                            <td><?= $key ?></td>
                            <td><?= $value ?></td>
                            <td><a href="/lr7-4-record.php?key=<?= $key ?>">Изменить</a> | <a href="/lr7-4-record.php?delete=<?= $key ?>">Удалить</a></td>
                        </tr>
					<?php } ?>
                    </tbody>
                </table>
                <a href="/lr7-4-record.php"><button>Добавить Cookie</button></a>
			</main>
		</div>
		<?php include 'inc/footer.inc'; ?>
	</div>
</body>
</html>