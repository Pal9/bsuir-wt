<?php $title = 'Добавление данных о студенте';

require_once 'inc/db.inc';

$key = (string) $_GET['key'];
$deleteByKey = (string) $_GET['delete'];
if (!empty($_POST)) {

    // из имени куки удаляем невалидные символы
	$oldKey = (string) $_POST['old_key'];
	$key = str_replace(['=', ',', ';', ' ', "\t", "\r", "\n", "\013", "\014"], '', (string) $_POST['key']);
	$value = (string) $_POST['value'];

	if (!empty($oldKey) && $oldKey != $key) { // при редактировании изменён ключ куки, нужно удалить старую запись, чтобы не было дубля
		setcookie($oldKey, null, -1);
    }

	// 86400 = 1 день, т.е. сохраняем на год (брауерные ограничения м.б. меньше)
	if (setcookie($key, $value, time() + (86400 * 365))) {
		// перенаправляем на общую страницу
		header('Location: /lr7-4.php');
		header('HTTP/1.1 302');
		die();
	} else {
		// что-то пошло не так
		$message = '<div class="error-message">Ошибка при добавлении Cookie.</div>';
	}
} elseif ($key) {
	$value = $_COOKIE[$key];
} elseif ($deleteByKey) {
	if (setcookie($deleteByKey, null, -1)) { // выставляем отрицательное время, чтобы браузер очистил запись
		// перенаправляем на общую страницу
		header('Location: /lr7-4.php');
		header('HTTP/1.1 302');
		die();
	} else {
		// что-то пошло не так
		$message = '<div class="error-message">Ошибка при удалении записи.</div>';
	}
}
?>
<!doctype html>
<html lang="ru">
<head>
	<?php include 'inc/head.inc'; ?>
	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<?php include 'inc/header.inc'; ?>
		<div class="main-row">
			<?php include 'inc/nav.inc'; ?>
			<main>
				<h1><?= $title ?></h1>
                <p>Заполните форму ниже для добавления Cookie.</p>
				<?php if (!empty($message)) {
					echo $message;
				} ?>
                <form method="post">
                    <table>
                        <thead>
                        <tr>
                            <th><label for="key">Ключ</label></th>
                            <th><label for="value">Значение</label></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><input type="text" maxlength="255" name="key" id="key" required value="<?= $key ?>"></td>
                            <td><textarea name="value" id="value" required><?= $value ?></textarea></td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="hidden" name="old_key" value="<?= $key ?>">
                    <input type="submit" value="Сохранить Cookie">
                </form>
			</main>
		</div>
		<?php include 'inc/footer.inc'; ?>
	</div>
</body>
</html>