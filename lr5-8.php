<?php $title = 'Лабораторная работа №5, вариант 8';
require_once 'inc/functions.inc';
require_once 'inc/db.inc';
// подготовим данные
$res = $db->query("SELECT * FROM students");

$students = []; // тут соберутся все необходимые данные о студентах

while ($student = $res->fetch_assoc()) {
    // рассчитываем средний балл студента
	$student['avg'] = ($student['math'] + $student['web'] + $student['history'] + $student['philosophy'] + $student['english']) / 5;

	// получаем список предметов, по которым у студента максимальный балл
	$student['max'] = max([$student['math'], $student['web'], $student['history'], $student['philosophy'], $student['english']]);
	$student['min'] = min([$student['math'], $student['web'], $student['history'], $student['philosophy'], $student['english']]);

	$student['maxDisciplines'] = matchDisciplines($student, $student['max']);
	$student['minDisciplines'] = matchDisciplines($student, $student['min']);

    $students[] = $student;
}
?>
<!doctype html>
<html lang="ru">
<head>
	<?php include 'inc/head.inc'; ?>
	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<?php include 'inc/header.inc'; ?>
		<div class="main-row">
			<?php include 'inc/nav.inc'; ?>
			<main>
				<h1><?= $title ?></h1>
                <h2>Задание:</h2>
                <p>Сформировать БД, содержащую информацию о студентах и их оценках по различным предметам (не менее пяти). Написать скрипт, формирующий список студентов с их средними баллами, а также минимальным и максимальным баллом с указанием списка предметов, за которые был получен такой балл. Предусмотреть корректировку по баллам для студента. Отобразить полный список и получившийся.</p>
                <h2>Решение:</h2>
                <p>Таблицы с заданными данными. В них предусмотрены ссылки для изменения, удаления и добавления данных.</p>
                <h3>Исходные данные</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Имя</th>
                        <th>Математика</th>
                        <th>Веб-технологии</th>
                        <th>История</th>
                        <th>Философия</th>
                        <th>Английский</th>
                        <th>Управление данными</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($students as $student) { ?>
                        <tr>
                            <td><?= $student['name'] ?></td>
                            <td><?= $student['math'] ?></td>
                            <td><?= $student['web'] ?></td>
                            <td><?= $student['history'] ?></td>
                            <td><?= $student['philosophy'] ?></td>
                            <td><?= $student['english'] ?></td>
                            <td><a href="/lr5-8-record.php?id=<?= $student['id'] ?>">Изменить</a> | <a href="/lr5-8-record.php?delete=<?= $student['id'] ?>">Удалить</a></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <a href="/lr5-8-record.php"><button>Добавить данные о студенте</button></a>
                <h3>Расчётные данные</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Имя</th>
                        <th>Средний балл</th>
                        <th>Максимальный балл</th>
                        <th>Максимальный балл по:</th>
                        <th>Минимальный балл</th>
                        <th>Минимальный балл по:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($students as $student) { ?>
                        <tr>
                            <td><?= $student['name'] ?></td>
                            <td><?= $student['avg'] ?></td>
                            <td><?= $student['max'] ?></td>
                            <td><?= implode(', ', $student['maxDisciplines']) ?></td>
                            <td><?= $student['min'] ?></td>
                            <td><?= implode(', ', $student['minDisciplines']) ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
			</main>
		</div>
		<?php include 'inc/footer.inc'; ?>
	</div>
</body>
</html>