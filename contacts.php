<?php $title = 'Контактные данные'; ?>
<!doctype html>
<html lang="ru">
<head>
	<?php include 'inc/head.inc'; ?>
	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<?php include 'inc/header.inc'; ?>
		<div class="main-row">
			<?php include 'inc/nav.inc'; ?>
			<main>
				<h1><?= $title ?></h1>
                <ul>
                    <li>Кольцов Андрей Витальевич</li>
                    <li><a href="tel:+375291320153">+375 (29) 132-01-53</a></li>
                    <li><a href="mailto:pal9yni4bi@gmail.com">pal9yni4bi@gmail.com</a></li>
                    <li>г. Минск, ул. Кирова, д. 16</li>
                </ul>
			</main>
		</div>
		<?php include 'inc/map.inc'; ?>
		<?php include 'inc/footer.inc'; ?>
	</div>
</body>
</html>