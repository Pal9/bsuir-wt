<div class="good-item">
    {{if(img)}}
    <img src="{{=img}}" alt="{{=title}}" class="adaptive-img">
    {{else}}
    <img src="/img/default.jpg" alt="" class="adaptive-img">
    {{endif}}

    {{if(title)}}
    <h3 class="good-item__title">{{=title}}</h3>
    {{endif}}

    {{if(price)}}
    <div class="good-item__price">{{=price}}</div>
    {{else}}
    <div class="good-item__price">Цена формируется</div>
    {{endif}}

</div>