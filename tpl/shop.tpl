<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/style.css">
    <title>{{=title}}</title>
</head>
<body>
    <div class="wrapper">
        <h1>{{=title}}</h1>
        {{if(description)}}
        {{=description}}
        {{else}}
        <p>Эта надпись видна, если не заполнено описание (description) при вызове шаблона.</p>
        {{endif}}

        {{if(goods)}}
        <div>
            {{foreach(goods)}}
            {{include(good){{=value}}}}
            {{endforeach}}
        </div>
        {{else}}
        <p>Не передано данных о товарах</p>
        {{endif}}
        <p>Футер выводится из подключаемого шаблона с пробросом стандартного массива переменных без переопределения.</p>
        {{include(footer)}}
    </div>
</body>
</html>