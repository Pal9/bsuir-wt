<?php $title = 'Лабораторная работа №3, вариант 7'; ?>
<?php include 'inc/functions.inc'; ?>
<!doctype html>
<html lang="ru">
<head>
	<?php include 'inc/head.inc'; ?>
	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<?php include 'inc/header.inc'; ?>
		<div class="main-row">
			<?php include 'inc/nav.inc'; ?>
			<main>
				<h1><?= $title ?></h1>
                <h2>Задание:</h2>
                <p>Написать функцию, формирующую полный список файлов и подкаталогов в указанном каталоге. Для всех элементов списка выводить размер в килобайтах (для подкаталогов считать размер их содержимого), дату и время создания, модификации и последнего обращения, а также иконку. Для всех текстовых файлов отобразить первые 100 символов. А также написать функцию, определяющую процентное отношение объёма графических файлов в произвольном каталоге (включая подкаталоги) к общему объёму данных в этом каталоге. Имя анализируемого каталога получать через веб-форму.</p>
                <h2>Решение:</h2>
                <p>Введите адрес каталога в поле ниже и нажмите кнопку "Анализировать".</p>
                <?php
                $pathInfo = [];
				if (!empty($_POST)) {
				    $path = trim((string) $_POST['path']);

				    if (empty($path)) {
				        echo '<div class="error-message">Не указан путь к анализируемому каталогу! Заполните форму корректно!</div>';
                    } else {
				        if (file_exists($path) && is_dir($path)) {
				            // получаем список всех файлов и папок в указанном каталоге, отбрасывая ".." и "."
				            $fileList = array_diff(scandir($path), ['..', '.']);

				            // инициализируем накопитель данных о раззмере анализируемой директории
                            $sizeOfTargetPath = 0;

				            foreach ($fileList as $filePath) {
				                $tmpInfo = [];
				                $fullPath = $path . DIRECTORY_SEPARATOR . $filePath;
				                $stat = stat($fullPath);

				                $tmpInfo['name'] = $filePath;
				                $tmpInfo['type'] = mime_content_type($fullPath);
				                $tmpInfo['size'] = targetSize($fullPath);
				                $tmpInfo['date_create'] = $stat['ctime'];
				                $tmpInfo['date_modify'] = $stat['mtime'];
				                $tmpInfo['date_access'] = $stat['atime'];
				                if ($tmpInfo['type'] === 'text/plain') {
				                    $handle = fopen($path . DIRECTORY_SEPARATOR . $filePath, 'r');
					                $tmpInfo['preview'] = mb_substr(fgets($handle), 0, 100);
                                } else {
					                $tmpInfo['preview'] = '';
                                }

					            $sizeOfTargetPath += $tmpInfo['size']; // подсчитываем суммарный объём содержимого целевой папки

					            $pathInfo[] = $tmpInfo;
                            }

					        $sizeOfImagesInTargetPath = imagesSize($path);
					        echo '<div class="ok-message">Графические файлы составляют ' . round($sizeOfImagesInTargetPath / $sizeOfTargetPath * 100, 2) . '% от объёма всех файлов в каталоге (' . $sizeOfImagesInTargetPath . ' кб из ' . $sizeOfTargetPath . ' кб)</div>';
                        } else {
					        echo '<div class="error-message">Указанный каталог не существует, укажите другой путь.</div>';
                        }
                    }
                }
				?>
                <form method="post" enctype="multipart/form-data" class="pretty-form">
                    <p><label for="path">Абсолютный путь к исследуемому каталогу:</label><input name="path" id="path" placeholder="Например, C:\Users" value="<?= $path ?>" required></p>
                    <p><input type="submit" value="Анализировать"></p>
                </form>
                <?php if (!empty($pathInfo)) { ?>
                    <table>
                        <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Тип</th>
                            <th>Размер, кб</th>
                            <th>Дата и время создания</th>
                            <th>Дата и время модификации</th>
                            <th>Дата и время последнего обращения</th>
                            <th>Превью текстовых файлов</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($pathInfo as $path) { ?>
                            <tr>
                                <td><?= $path['name'] ?></td>
                                <td><?= $path['type'] ?></td>
                                <td><?= $path['size'] ?></td>
                                <td><?= date('Y-m-d H:i:s', $path['date_create']) ?></td>
                                <td><?= date('Y-m-d H:i:s', $path['date_modify']) ?></td>
                                <td><?= date('Y-m-d H:i:s', $path['date_access']) ?></td>
                                <td><?= $path['preview'] ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                <?php } ?>
			</main>
		</div>
		<?php include 'inc/footer.inc'; ?>
	</div>
</body>
</html>