<?php $title = 'Новости'; ?>
<!doctype html>
<html lang="ru">
<head>
	<?php include 'inc/head.inc'; ?>
	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<?php include 'inc/header.inc'; ?>
		<div class="main-row">
			<?php include 'inc/nav.inc'; ?>
			<main>
				<h1><?= $title ?></h1>
                <div class="news-preview">
                    <h2 class="news-preview__header">Открытие месторождения нефти</h2>
                    <p class="news-preview__text">Сегодня, в 4 корпусе БГУИР было открыто новое месторождение нефти. Начальник главного управления...</p>
                    <a class="news-preview__link" href="/news-3.php">Подробнее...</a>
                </div>
                <div class="news-preview">
                    <h2 class="news-preview__header">Открытие 4 корпуса</h2>
                    <p class="news-preview__text">Сегодня состоялось торженственное открытие 4 корпус БГУИР. Виновные наказаны. Уже завтра они...</p>
                    <a class="news-preview__link" href="/news-2.php">Подробнее...</a>
                </div>
                <div class="news-preview">
                    <h2 class="news-preview__header">Плутон больше не планета!</h2>
                    <p class="news-preview__text">С превеликим прискорбием вынуждены сообщить пренеприятнейшее известие: в результате последних достижений астрономической науки...</p>
                    <a class="news-preview__link" href="/news-1.php">Подробнее...</a>
                </div>
			</main>
		</div>
		<?php include 'inc/footer.inc'; ?>
	</div>
</body>
</html>