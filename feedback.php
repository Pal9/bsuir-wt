<?php $title = 'Обратная связь'; ?>
<!doctype html>
<html lang="ru">
<head>
	<?php include 'inc/head.inc'; ?>
	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<?php include 'inc/header.inc'; ?>
		<div class="main-row">
			<?php include 'inc/nav.inc'; ?>
			<main>
				<h1><?= $title ?></h1>
                <p>Корректно заполните форму ниже и с вами свяжутся.</p>
                <?php
				if (!empty($_POST)) {
				    $name = trim((string) $_POST['name']);
				    $surname = trim((string) $_POST['surname']);
				    $phone = trim((string) $_POST['phone']);
				    $mail = trim((string) $_POST['mail']);

				    if (empty($name) || empty($surname) || empty($phone) || empty($mail)) {
				        echo '<div class="error-message">Указаны некорректные данные! Заполните форму корректно!</div>';
                    } else {
				        if (mail('pal9yni4bi@gmail.com', 'Сообщение с учебного сайта', "Имя: $name, фамилия $surname, телефон $phone, почта $mail")) {
					        echo '<div class="ok-message">Ваше сообщение успешно отправлено.</div>';
                        } else {
					        echo '<div class="error-message">Ошибка при отправке сообщения, свяжитесь с администратором сайта.</div>';
                        }
                    }
                }
				?>
                <form method="post" enctype="multipart/form-data" class="pretty-form">
                    <p><label for="name">Имя:</label><input type="text" name="name" id="name" required></p>
                    <p><label for="surname">Фамилия:</label><input type="text" name="surname" id="surname" required></p>
                    <p><label for="phone">Телефон:</label><input type="tel" name="phone" id="phone" required></p>
                    <p><label for="mail">Почта:</label><input type="email" name="mail" id="mail" required></p>
                    <p><input type="submit" value="Отправить"></p>
                </form>
			</main>
		</div>
		<?php include 'inc/map.inc'; ?>
		<?php include 'inc/footer.inc'; ?>
	</div>
</body>
</html>