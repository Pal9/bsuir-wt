<?php $title = 'Лабораторная работа №2, вариант 3'; ?>
<!doctype html>
<html lang="ru">
<head>
	<?php include 'inc/head.inc'; ?>
	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<?php include 'inc/header.inc'; ?>
		<div class="main-row">
			<?php include 'inc/nav.inc'; ?>
			<main>
				<h1><?= $title ?></h1>
                <h2>Задание:</h2>
                <p>В произвольном тексте каждое третье слово перевести в верхний регистр, каждую третью букву всех слов сделать фиолетовой, подсчитать общее количество встречающихся в тексте букв "о" и "О".</p>
                <h2>Решение:</h2>
                <p>Введите текст в поле ниже и нажмите кнопку "преобразовать".</p>
                <?php
				if (!empty($_POST)) {
				    $text = trim((string) $_POST['text']);

				    if (empty($text)) {
				        echo '<div class="error-message">Указаны некорректные данные! Заполните форму корректно!</div>';
                    } else {
				        // пришёл некоторый текст, производим преобразования в соответствии с заданием
                        // 1) посчитаем количество вхождений в текст букв "о" и "О", подразумеваем,
                        // что в задании речь только про кириллические символы

                        $count = mb_substr_count($text, 'о') + mb_substr_count($text, 'О');
					    echo '<div class="ok-message">Число вхождений букв "о" и "О": ' . $count . '</div>';

					    // удалим все двойные пробелы для корректного определения слов
					    $replaceCounter = 0;
					    do {
						    $text = str_replace('  ', ' ', $text, $replaceCounter);
                        } while ($replaceCounter > 0);

					    $explodedText = explode(' ', $text);
					    foreach ($explodedText as $key => $word) {
					        if (($key + 1) % 3 === 0) {
					            // 2) в соответствии с заданием, каждое третье слово переводим в верхний регистр
						        $word = mb_strtoupper($word);
                            }

					        $explodedWord = preg_split('//u', $word, null, PREG_SPLIT_NO_EMPTY);
                            if (count($explodedWord) > 3) {
	                            $tmp = '';
	                            // 2) в соответствии с заданием, каждую третью букву делаем фиолетовой
                                foreach ($explodedWord as $letterKey => $letter) {
	                                if (($letterKey + 1) % 3 === 0) {
		                                $tmp .= '<strong class="violet">' . $letter . '</strong>';
	                                } else {
		                                $tmp .= $letter;
	                                }
                                }
	                            $word = $tmp;
                            }

						    // сохраняем изменения (для стандартного текста выгоднее вынести это присваивание из условий)
						    $explodedText[$key] = $word;
                        }

					    $text = implode(' ', $explodedText); // преобразуем текст обратно в строку

					    echo '<div>Преобразованный текст:<br>' . $text . '</div>';
                    }
                }
				?>
                <form method="post" enctype="multipart/form-data" class="pretty-form">
                    <p><textarea name="text" id="text" placeholder="Введите текст для обработки в это поле..." required></textarea></p>
                    <p><input type="submit" value="Преобразовать"></p>
                </form>
			</main>
		</div>
		<?php include 'inc/footer.inc'; ?>
	</div>
</body>
</html>