<?php $title = 'Главная'; ?>
<!doctype html>
<html lang="ru">
<head>
	<?php include 'inc/head.inc'; ?>
	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<?php include 'inc/header.inc'; ?>
		<div class="main-row">
			<?php include 'inc/nav.inc'; ?>
			<main>
				<h1>Главная страница сайта.</h1>
				<p>Вы находитесь на главной странице сайта, созданного в качестве учебного.</p>
			</main>
			<?php include 'inc/news.inc'; ?>
		</div>
		<?php include 'inc/footer.inc'; ?>
	</div>
</body>
</html>