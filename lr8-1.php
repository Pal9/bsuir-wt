<?php
include 'inc/functions.inc';
include 'inc/db.inc';
$title = 'Лабораторная работа №8, вариант 1';

$res = $db->query("SELECT `browser`, `count` FROM `browsers` ORDER BY `count` DESC");

$browsers = [];
if ($res) {
	while ($row = $res->fetch_assoc()) {
		$browsers[$row['browser']] = $row['count'];
	}
}

$browser = (string) getBrowser($_SERVER['HTTP_USER_AGENT']);
if (!isset($_COOKIE['browser_counter'])) { // браузер ещё не участвовал в статистике
	if (isset($browsers[$browser])) { // это не первый посетитель с данного браузера, просто увеличиваем счётчик
		$browsers[$browser]++; // для корректных данных в таблице
		$res = $db->query("UPDATE `browsers` SET `count` = `count` + 1 WHERE `browser` = '$browser'");
	} else { // это первый посетитель с данного браузера, добавляем новую запись
		$browsers[$browser] = 1; // для корректных данных в таблице
		$res = $db->query("INSERT INTO `browsers` (`browser`, `count`) VALUES ('$browser', 1)");
    }

    if ($res) {
	    // если информация о посетителе успешно записана в БД, то отмечаем пользователя кукой,
        // чтобы не учитывать его повторно в статистике
	    setcookie('browser_counter', '1', time() + 86400 * 365);
    }
}
?>
<!doctype html>
<html lang="ru">
<head>
	<?php include 'inc/head.inc'; ?>
	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<?php include 'inc/header.inc'; ?>
		<div class="main-row">
			<?php include 'inc/nav.inc'; ?>
			<main>
				<h1><?= $title ?></h1>
                <h2>Задание:</h2>
                <p>Написать скрипт, собирающий статистику по используемым посетителями ресурса браузерам. Выводить результаты в виде HTML-таблицы со списком браузеров, отсортированным по убыванию количества пользующихся ими посетителей сайта.</p>
                <h2>Решение:</h2>
                <p>Получить данные об используемом браузере пользователя можно анализируя информацию в $_SERVER['HTTP_USER_AGENT']. Данные хранятся в БД MySQL.</p>
                <p>Ваш браузер определён как <?= $browser ?> на основании вашего user-agent: <?= $_SERVER['HTTP_USER_AGENT']?></p>
                <?php if (count($browsers) > 0) { ?>
                    <p>А вот статистика всех пользователей:</p>
                    <table>
                        <thead>
                        <tr>
                            <th>Браузер</th>
                            <th>Количество уникальных посетителей</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($browsers as $browser => $count) { ?>
                            <tr>
                                <td><?= $browser ?></td>
                                <td><?= $count ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                <?php } ?>
			</main>
		</div>
		<?php include 'inc/footer.inc'; ?>
	</div>
</body>
</html>