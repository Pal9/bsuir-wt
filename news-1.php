<?php $title = 'Плутон больше не планета!'; ?>
<!doctype html>
<html lang="ru">
<head>
	<?php include 'inc/head.inc'; ?>
	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<?php include 'inc/header.inc'; ?>
		<div class="main-row">
			<?php include 'inc/nav.inc'; ?>
			<main>
				<h1><?= $title ?></h1>
                <p>С превеликим прискорбием вынуждены сообщить пренеприятнейшее известие: в результате последних достижений астрономической науки выдающиеся умы планеты приняли решение о разжаловании Плутона из статуса планеты в статус карликовой планеты.</p>
                <p>Уверены, пройдут десятки лет, и вопрос о настоящем статусе этого ледяного мира на задворках Солнечной системы будет поднят вновь.</p>
                <img src="/img/pluto.jpg" alt="Плутон глазами зонда Dawn" title="Плутон снятый зондом Dawn" class="adaptive-img">
			</main>
		</div>
		<?php include 'inc/footer.inc'; ?>
	</div>
</body>
</html>