<?php $title = 'Добавление данных о студенте';

require_once 'inc/db.inc';

$id = (int) $_GET['id'];
$deleteById = (int) $_GET['delete'];
if (!empty($_POST)) {

	$id = (int) $_POST['id'];
	$name = $db->real_escape_string(trim((string) $_POST['name']));
	$math = (int) $_POST['math'];
	$web = (int) $_POST['web'];
	$history = (int) $_POST['history'];
	$philosophy = (int) $_POST['philosophy'];
	$english = (int) $_POST['english'];

	if (empty($name)) {
		$message = '<div class="error-message">Необходимо указать имя студента!</div>';
	} else {
	    if ($id) { // обновление записи
		    $res = $db->query("UPDATE `students` SET `name` = '$name', `math` = $math, `web` = $web, `history` = $history, `philosophy` = $philosophy, `english` = $english WHERE `id` = $id");
        } else { // добавление записи
		    $res = $db->query("INSERT INTO `students`
                                                  (`name`, `math`, `web`, `history`, `philosophy`, `english`) 
                                                  VALUES ('$name', $math, $web, $history, $philosophy, $english)");
        }

		if ($res) {
			// перенаправляем на общую страницу
			header('Location: /lr5-8.php');
			header('HTTP/1.1 302');
			die();
		} else {
			// что-то пошло не так
			$message = '<div class="error-message">Ошибка при добавлении в базу данных.</div>';
		}
	}
} elseif ($id) {
	$res = $db->query("SELECT * FROM students WHERE id = $id");
	$student = $res->fetch_assoc();

	$name = $student['name'];
	$math = $student['math'];
	$web = $student['web'];
	$history = $student['history'];
	$philosophy = $student['philosophy'];
	$english = $student['english'];
} elseif ($deleteById) {
	$res = $db->query("DELETE FROM students WHERE id = $deleteById");
	if ($res) {
		// перенаправляем на общую страницу
		header('Location: /lr5-8.php');
		header('HTTP/1.1 302');
		die();
	} else {
		// что-то пошло не так
		$message = '<div class="error-message">Ошибка при удалении записи.</div>';
	}
}
?>
<!doctype html>
<html lang="ru">
<head>
	<?php include 'inc/head.inc'; ?>
	<title><?= $title ?></title>
</head>
<body>
	<div class="wrapper">
		<?php include 'inc/header.inc'; ?>
		<div class="main-row">
			<?php include 'inc/nav.inc'; ?>
			<main>
				<h1><?= $title ?></h1>
                <p>Заполните форму ниже для добавления данных об оценках нового студента.</p>
                <h3>Исходные данные</h3>
				<?php if (!empty($message)) {
					echo $message;
				} ?>
                <form method="post">
                    <table class="add-student">
                        <thead>
                        <tr>
                            <th><label for="name">Имя</label></th>
                            <th><label for="math">Математика</label></th>
                            <th><label for="web">Веб-технологии</label></th>
                            <th><label for="history">История</label></th>
                            <th><label for="philosophy">Философия</label></th>
                            <th><label for="english">Английский</label></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><input type="text" maxlength="255" name="name" id="name" required value="<?= $name ?>"></td>
                            <td><input type="number" min="0" max="10" name="math" id="math" required value="<?= $math ?>"></td>
                            <td><input type="number" min="0" max="10" name="web" id="web" required value="<?= $web ?>"></td>
                            <td><input type="number" min="0" max="10" name="history" id="history" required value="<?= $history ?>"></td>
                            <td><input type="number" min="0" max="10" name="philosophy" id="philosophy" required value="<?= $philosophy ?>"></td>
                            <td><input type="number" min="0" max="10" name="english" id="english" required value="<?= $english ?>"></td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="hidden" name="id" value="<?= $id ?>">
                    <input type="submit" value="Сохранить данные о студенте">
                </form>
			</main>
		</div>
		<?php include 'inc/footer.inc'; ?>
	</div>
</body>
</html>